// class work
function Calculator() {
    var a;
    var b;
    this.read = function(){
        this.a  = parseInt(prompt('a = ?'));
        this.b = parseInt(prompt('b = ?'));
    }
    this.sum =function() {
        return this.a + this.b;
    }
    this.mul =function() {
        return this.a * this.b;
    }
}


// home work
function Human(name, age) {
    this.name = name;
    this.age = age;
    this.fingers = 20;
    this.mammal = true;
}

var humanDasha = new Human('Dasha', 76);
var humanPasha = new Human('Pasha', 19);
var humanGrisha = new Human('Grisha', 42);
var humanFourth = new Human('Fourth', 34);
var arrObj = [humanDasha, humanPasha, humanGrisha, humanFourth];

function sortByAge(arr) {
    arr.sort(function(a, b){
       return a.age - b.age;
    } );
}

sortByAge(arrObj);